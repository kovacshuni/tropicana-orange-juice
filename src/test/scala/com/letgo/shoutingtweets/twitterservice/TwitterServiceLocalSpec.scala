package com.letgo.shoutingtweets.twitterservice

import scala.concurrent.Future

import org.specs2.concurrent.ExecutionEnv
import org.specs2.mock.Mockito
import org.specs2.mutable.Specification

import com.letgo.shoutingtweets.domain.Tweet
import com.letgo.shoutingtweets.infrastructure.TweetRepository

class TwitterServiceLocalSpec(implicit ee: ExecutionEnv) extends Specification with Mockito {

  "TwitterService" should {
    "return TweetRepository's result" in {
      val username = "johndoe"
      val count = 2
      val mockedTweetRepository = mock[TweetRepository]
      mockedTweetRepository.searchByUserName(username, count) returns Future.successful(Seq(Tweet("a"), Tweet("b")))

      val twitterService = new TwitterServiceLocal(mockedTweetRepository)
      twitterService.searchByUserName(username, count) must equalTo(Seq(Tweet("a"), Tweet("b"))).await

      there was one(mockedTweetRepository).searchByUserName(username, count)
    }
  }

  "TwitterService" should {
    "wrap TweetRepository's execptions and forward them back" in {
      val username = "johndoe"
      val count = 2
      val mockedTweetRepository = mock[TweetRepository]
      mockedTweetRepository.searchByUserName(username, count) returns
        Future.failed(new RuntimeException("integration cause"))

      val twitterService = new TwitterServiceLocal(mockedTweetRepository)
      twitterService.searchByUserName(username, count) must
        throwA[TwitterServiceException](message = "Failed to fetch tweets from repository. username=johndoe count=2")
          .await

      there was one(mockedTweetRepository).searchByUserName(username, count)
    }
  }
}
