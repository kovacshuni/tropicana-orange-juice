package com.letgo.shoutingtweets.twitterservice

import scala.concurrent.Future

import akka.http.caching.LfuCache
import akka.http.caching.scaladsl.{Cache, CachingSettings}
import com.typesafe.config.ConfigFactory
import org.specs2.concurrent.ExecutionEnv
import org.specs2.mock.Mockito
import org.specs2.mutable.Specification

import com.letgo.shoutingtweets.domain.Tweet

class TwitterServiceCachedSpec(implicit ee: ExecutionEnv) extends Specification with Mockito {

  "TwitterServiceCached" should {
    "call for underlying service if cached result was present but less tweets are preserved than needed" in {
      val username = "johndoe"
      val count = 3
      val cachingSettings = CachingSettings(ConfigFactory.defaultApplication())
      val cache: Cache[String, Seq[Tweet]] = LfuCache(cachingSettings)
      val mockedTwitterService: TwitterService = mock[TwitterService]
      mockedTwitterService.searchByUserName(username, count) returns
        Future.successful(Seq(Tweet("a"), Tweet("b"), Tweet("c")))
      cache.getOrLoad(username, _ => Future.successful(Seq(Tweet("a"), Tweet("b"))))
      val cachedTS = new TwitterServiceCached(mockedTwitterService, cache)

      cachedTS.searchByUserName(username, count) must equalTo(Seq(Tweet("a"), Tweet("b"), Tweet("c"))).await

      there was one(mockedTwitterService).searchByUserName(username, count)
    }

    "not call for underlying service if more tweets are present in the cache" in {
      val username = "johndoe"
      val cachingSettings = CachingSettings(ConfigFactory.defaultApplication())
      val cache: Cache[String, Seq[Tweet]] = LfuCache(cachingSettings)
      val mockedTwitterService: TwitterService = mock[TwitterService]
      cache.getOrLoad(username, _ => Future.successful(Seq(Tweet("a"), Tweet("b"), Tweet("c"))))
      val cachedTS = new TwitterServiceCached(mockedTwitterService, cache)

      cachedTS.searchByUserName(username, 2) must equalTo(Seq(Tweet("a"), Tweet("b"))).await

      there was no(mockedTwitterService).searchByUserName(anyString, anyInt)
    }

    "throw if underlying service throws" in {
      val username = "johndoe"
      val count = 2
      val cachingSettings = CachingSettings(ConfigFactory.defaultApplication())
      val cache: Cache[String, Seq[Tweet]] = LfuCache(cachingSettings)
      val mockedTwitterService: TwitterService = mock[TwitterService]
      mockedTwitterService.searchByUserName(username, count) throws TwitterServiceException("Underlying service error")
      val cachedTS = new TwitterServiceCached(mockedTwitterService, cache)

      cachedTS.searchByUserName(username, count) must
        throwA[TwitterServiceException](message = "Underlying service error")

      there was one(mockedTwitterService).searchByUserName(username, count)
    }
  }
}
