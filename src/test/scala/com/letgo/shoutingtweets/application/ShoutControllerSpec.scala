package com.letgo.shoutingtweets.application;

import scala.concurrent.Future
import akka.http.scaladsl.server._
import akka.http.scaladsl.testkit.Specs2RouteTest
import org.specs2.mock.Mockito
import org.specs2.mutable.Specification
import Directives._
import akka.http.scaladsl.model.StatusCodes.{BadRequest, MethodNotAllowed, NotFound, OK, ServiceUnavailable}
import com.letgo.shoutingtweets.twitterservice.TwitterServiceException

class ShoutControllerSpec extends Specification with Specs2RouteTest with Mockito {

  "The controller" should {

    "return 2 lines for GET /shout/johndoe?limit=2" in {
      val mockedTweetShouter = mock[TweetShouter]
      mockedTweetShouter.shout("johndoe", 2) returns Future.successful(Seq("A!", "B!"))
      val route = new ShoutController(mockedTweetShouter).route

      Get("/shout/johndoe?limit=2") ~> route ~> check {
        status shouldEqual OK
        responseAs[String] shouldEqual "[\n\tA!\n\tB!\n]"
      }

      there was one(mockedTweetShouter).shout("johndoe", 2)
    }

    "return Bad Request for GET /shout/johndoe?limit=11" in {
      val mockedTweetShouter = mock[TweetShouter]
      mockedTweetShouter.shout("johndoe", 11) throws CountExceededException()
      val route = new ShoutController(mockedTweetShouter).route

      Get("/shout/johndoe?limit=11") ~> route ~> check {
        status shouldEqual BadRequest
        responseAs[String] shouldEqual "Count must be between [0, 10]"
      }

      there was one(mockedTweetShouter).shout("johndoe", 11)
    }

    "return Bad Request for GET /shout/bad|username?limit=2" in {
      val mockedTweetShouter = mock[TweetShouter]
      mockedTweetShouter.shout("bad|username", 2) throws UsernameMalformedException("Malformed username")
      val route = new ShoutController(mockedTweetShouter).route

      Get("/shout/bad|username?limit=2") ~> route ~> check {
        status shouldEqual BadRequest
        responseAs[String] shouldEqual "Malformed username"
      }

      there was one(mockedTweetShouter).shout("bad|username", 2)
    }

    "return NotFound for GET /inexistent/path" in {
      val mockedTweetShouter = mock[TweetShouter]
      val route = new ShoutController(mockedTweetShouter).route

      Get("/inexistent/path") ~> route ~> check {
        status shouldEqual NotFound
      }
    }

    "return MethodNotAllowed for POST /shout/johndoe?limit=11" in {
      val mockedTweetShouter = mock[TweetShouter]
      val route = new ShoutController(mockedTweetShouter).route

      Post("/shout/johndoe?limit=2") ~> route ~> check {
        status shouldEqual MethodNotAllowed
        responseAs[String] shouldEqual "Method not allowed. Supported methods are: GET"
      }
    }

    "return ServiceUnavailable for GET /shout/johndoe?limit=2 when internal errors occur" in {
      val mockedTweetShouter = mock[TweetShouter]
      mockedTweetShouter.shout("johndoe", 2) returns Future.failed(TwitterServiceException("Error in dependencies"))
      val route = new ShoutController(mockedTweetShouter).route

      Get("/shout/johndoe?limit=2") ~> route ~> check {
        status shouldEqual ServiceUnavailable
        responseAs[String] shouldEqual "Error in underlying services. Please try again later."
      }

      there was one(mockedTweetShouter).shout("johndoe", 2)
    }
  }
}
