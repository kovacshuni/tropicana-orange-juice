package com.letgo.shoutingtweets.application

import scala.concurrent.Future

import org.specs2.concurrent.ExecutionEnv
import org.specs2.mock.Mockito
import org.specs2.mutable.Specification

import com.letgo.shoutingtweets.domain.Tweet
import com.letgo.shoutingtweets.twitterservice.{TwitterService, TwitterServiceException}

class TweetShouterSpec(implicit ee: ExecutionEnv) extends Specification with Mockito {

  "TweetShouter" should {
    "turn tweets to uppercase, exclamation and concatenate" in {
      val mockedTwitterService = mock[TwitterService]
      mockedTwitterService.searchByUserName("johndoe", 2) returns Future.successful(List(Tweet("a"), Tweet("b")))
      val tweetShouter = new TweetShouter(mockedTwitterService)

      tweetShouter.shout("johndoe", 2) must beEqualTo(Seq("A!", "B!")).await

      there was one(mockedTwitterService).searchByUserName("johndoe", 2)
    }

    "de-duplicate exclamation marks and turn full stops to exclamation marks" in {
      val mockedTwitterService = mock[TwitterService]
      mockedTwitterService.searchByUserName("johndoe", 2) returns
        Future.successful(List(
          Tweet("Big announcement with my friend Ambassador Nikki Haley in the Oval Office at 10:30am."),
          Tweet("Will be going to Iowa tonight for Rally, and more! The Farmers (and all) are very happy with USMCA!")
        ))
      val tweetShouter = new TweetShouter(mockedTwitterService)

      tweetShouter.shout("johndoe", 2) must beEqualTo(Seq(
        "BIG ANNOUNCEMENT WITH MY FRIEND AMBASSADOR NIKKI HALEY IN THE OVAL OFFICE AT 10:30AM!",
        "WILL BE GOING TO IOWA TONIGHT FOR RALLY, AND MORE! THE FARMERS (AND ALL) ARE VERY HAPPY WITH USMCA!"
      )).await

      there was one(mockedTwitterService).searchByUserName("johndoe", 2)
    }

    "trim spaces" in {
      val mockedTwitterService = mock[TwitterService]
      mockedTwitterService.searchByUserName("johndoe", 1) returns Future.successful(List(Tweet("Affirmation. ")))
      val tweetShouter = new TweetShouter(mockedTwitterService)

      tweetShouter.shout("johndoe", 1) must beEqualTo(Seq("AFFIRMATION!")).await

      there was one(mockedTwitterService).searchByUserName("johndoe", 1)
    }

    "throw when count is not between [1, 10]" in {
      val mockedTwitterService = mock[TwitterService]
      val tweetShouter = new TweetShouter(mockedTwitterService)

      tweetShouter.shout("johndoe", -1) must throwA[CountExceededException].await
      tweetShouter.shout("johndoe", 11) must throwA[CountExceededException].await
    }

    "throw when username is empty" in {
      val mockedTwitterService = mock[TwitterService]
      val tweetShouter = new TweetShouter(mockedTwitterService)

      tweetShouter.shout("", 2) must throwA[UsernameMalformedException](message = "Username cannot be empty").await
    }

    "throw when username is too long" in {
      val mockedTwitterService = mock[TwitterService]
      val tweetShouter = new TweetShouter(mockedTwitterService)

      tweetShouter.shout("1234567890123456", 2) must
        throwA[UsernameMalformedException](message = "Malformed username").await
    }

    "throw when username has special characters (except _)" in {
      val mockedTwitterService = mock[TwitterService]
      val tweetShouter = new TweetShouter(mockedTwitterService)

      tweetShouter.shout("a=", 2) must throwA[UsernameMalformedException](message = "Malformed username").await
    }

    "throw when TwitterService throws" in {
      val mockedTwitterService = mock[TwitterService]
      mockedTwitterService.searchByUserName("johndoe", 2) returns
        Future.failed(TwitterServiceException("Failed to fetch tweets from repository. username=johndoe count=2"))
      val tweetShouter = new TweetShouter(mockedTwitterService)

      tweetShouter.shout("johndoe", 2) must throwA[TwitterServiceException](message =
        "Failed to fetch tweets from repository. username=johndoe count=2").await
    }
  }
}
