package com.letgo.shoutingtweets.domain

import akka.actor.ActorSystem
import akka.http.scaladsl.marshalling.{Marshal, ToEntityMarshaller}
import akka.http.scaladsl.model.ContentTypes.`text/plain(UTF-8)`
import akka.http.scaladsl.model.HttpEntity
import akka.stream.ActorMaterializer
import akka.util.ByteString
import org.specs2.concurrent.ExecutionEnv
import org.specs2.mutable.Specification

class TweetMarshallerSpec(implicit ee: ExecutionEnv) extends Specification {

  private val sys = ActorSystem("test")
  implicit private val mat: ActorMaterializer = ActorMaterializer()(sys)

  implicit private val marshaller: ToEntityMarshaller[Seq[String]] = TweetMarshaller.marshaller[Seq[String]]

  "TweetMarshaller marshalling tweets" should {
    "return Content-Type: text/plain" in {
      Marshal(Seq("a", "b")).to[HttpEntity].map(_.contentType) must
        beEqualTo(`text/plain(UTF-8)`).await
    }
  }

  "TweetMarshaller marshalling tweets" should {
    "return array as text with tabs and newlines" in {
      Marshal(Seq("a", "b")).to[HttpEntity] flatMap { httpEntity =>
        httpEntity.dataBytes.runFold(ByteString.empty)(_.concat(_)).map(_.utf8String)
      } must beEqualTo("[\n\ta\n\tb\n]").await
    }
  }

  "TweetMarshaller marshalling single tweet" should {
    "return array as text in standalone line" in {
      Marshal(Seq("a")).to[HttpEntity] flatMap { httpEntity =>
        httpEntity.dataBytes.runFold(ByteString.empty)(_.concat(_)).map(_.utf8String)
      } must beEqualTo("[\n\ta\n]").await
    }
  }

  "TweetMarshaller marshalling []" should {
    "return one line of []" in {
      Marshal(Seq.empty[String]).to[HttpEntity] flatMap { httpEntity =>
        httpEntity.dataBytes.runFold(ByteString.empty)(_.concat(_)).map(_.utf8String)
      } must beEqualTo("[]").await
    }
  }
}
