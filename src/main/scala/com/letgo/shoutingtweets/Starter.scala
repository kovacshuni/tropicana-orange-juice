package com.letgo.shoutingtweets

import scala.concurrent.Await
import scala.concurrent.duration._

import akka.actor.ActorSystem
import akka.http.caching.LfuCache
import akka.http.caching.scaladsl.{Cache, CachingSettings}
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import org.slf4j.LoggerFactory

import com.letgo.shoutingtweets.application.{ShoutController, TweetShouter}
import com.letgo.shoutingtweets.domain.Tweet
import com.letgo.shoutingtweets.infrastructure.TweetRepositoryInMemory
import com.letgo.shoutingtweets.twitterservice.{TwitterServiceCached, TwitterServiceLocal}

object Starter extends App {

  private val logger = LoggerFactory.getLogger(getClass)

  implicit val actorSystem: ActorSystem        = ActorSystem()
  implicit val materializer: ActorMaterializer = ActorMaterializer()(actorSystem)
  import actorSystem.dispatcher

  private val twitterRepository = new TweetRepositoryInMemory()

  private val twitterService = new TwitterServiceLocal(twitterRepository)

  private val cachingSettings = CachingSettings(actorSystem)
  private val cache: Cache[String, Seq[Tweet]] = LfuCache(cachingSettings)
  private val twitterServiceCached = new TwitterServiceCached(twitterService, cache)

  private val tweetShouter = new TweetShouter(twitterServiceCached)
  private val shoutController = new ShoutController(tweetShouter)

  private val port = 9000
  private val httpBinding = Http().bindAndHandle(shoutController.route, "0.0.0.0", port)
  logger.info(s"Server online, accessible on port=$port")
  logger.info("Press Ctrl-C (or send SIGINT) to stop.")
  scala.sys addShutdownHook shutdown

  private def shutdown() = {
    logger.info("Exiting...")
    Await.ready(
      httpBinding.flatMap(_.unbind())
        .flatMap(_ => Http().shutdownAllConnectionPools())
        .flatMap(_ => actorSystem.terminate()),
      5 seconds
    )
  }
}
