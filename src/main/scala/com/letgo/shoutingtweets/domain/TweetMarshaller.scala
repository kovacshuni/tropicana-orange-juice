package com.letgo.shoutingtweets.domain

import akka.http.scaladsl.marshalling.{Marshaller, ToEntityMarshaller}
import akka.http.scaladsl.model.ContentTypes.`text/plain(UTF-8)`
import akka.http.scaladsl.model.HttpEntity

object TweetMarshaller {

  def marshaller[T<: Seq[String]]: ToEntityMarshaller[T] = {
    Marshaller.withFixedContentType(`text/plain(UTF-8)`) { a: Seq[String] =>
      if (a.isEmpty) {
        HttpEntity(`text/plain(UTF-8)`, "[]")
      } else {
        val reduced = a.foldLeft("")((a, b) => a + "\n\t" + b)
        val parenthesized = "[" + reduced + "\n]"
        HttpEntity(`text/plain(UTF-8)`, parenthesized)
      }
    }
  }
}
