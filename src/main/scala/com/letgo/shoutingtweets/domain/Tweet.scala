package com.letgo.shoutingtweets.domain

case class Tweet(text: String)
