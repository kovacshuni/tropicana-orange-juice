package com.letgo.shoutingtweets.application

case class CountExceededException() extends RuntimeException
