package com.letgo.shoutingtweets.application

import scala.concurrent.{ExecutionContext, Future}

import com.letgo.shoutingtweets.domain.Tweet
import com.letgo.shoutingtweets.twitterservice.TwitterService

class TweetShouter(private val twitterService: TwitterService)
                  (implicit val ec: ExecutionContext) {

  private val TwitterUsernameRegexp = "^[a-zA-Z0-9_]{1,15}$"

  def shout(username: String, count: Int): Future[Seq[String]] = {
    if (count < 1 || count > 10) {
      Future.failed(CountExceededException())
    } else if (username.isEmpty) {
      Future.failed(UsernameMalformedException("Username cannot be empty"))
    } else if (!username.matches(TwitterUsernameRegexp)) {
      Future.failed(UsernameMalformedException("Malformed username"))
    } else {

      val tweetsF = twitterService.searchByUserName(username, count)
      tweetsF.map { tweets =>
        tweets.map(tweet => shoutTweet(tweet))
      }
    }
  }

  private def shoutTweet(tweet: Tweet): String = {
    val text = tweet.text.trim.toUpperCase()
    if (text.endsWith("!")) {
      text
    } else if (text.endsWith(".")) {
      text.substring(0, text.length - 1) + "!"
    } else {
      text + "!"
    }
  }
}
