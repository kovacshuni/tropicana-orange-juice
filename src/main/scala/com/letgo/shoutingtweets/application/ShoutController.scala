package com.letgo.shoutingtweets.application

import akka.http.scaladsl.marshalling.ToEntityMarshaller
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.StatusCodes.{BadRequest, MethodNotAllowed, NotFound, ServiceUnavailable}
import akka.http.scaladsl.server.Directives.{complete, get, handleExceptions, path, _}
import akka.http.scaladsl.server._
import org.slf4j.LoggerFactory

import com.letgo.shoutingtweets.domain.TweetMarshaller
import com.letgo.shoutingtweets.twitterservice.TwitterServiceException

class ShoutController(private val tweetShouter: TweetShouter) {

  private val logger = LoggerFactory.getLogger(getClass)

  private implicit val marshaller: ToEntityMarshaller[Seq[String]] = TweetMarshaller.marshaller[Seq[String]]

  private val exceptionHandler = ExceptionHandler {
    case _: CountExceededException =>
      complete(HttpResponse(status = BadRequest, entity = "Count must be between [0, 10]"))

    case UsernameMalformedException(message) =>
      complete(HttpResponse(status = BadRequest, entity = message))

    case tEx: TwitterServiceException =>
      logger.error(s"Error calling TwitterService: ${tEx.getMessage}", tEx)
      complete(HttpResponse(status = ServiceUnavailable,
        entity = "Error in underlying services. Please try again later."))

    case t: Throwable =>
      logger.error(s"Unexpected error: ${t.getMessage}", t)
      complete(s"Unexpected error: ${t.getMessage}")
  }

  private val rejectionHandler =
    RejectionHandler.newBuilder()
      .handleAll[MethodRejection] { methodRejections =>
      val names = methodRejections.map(_.supported.name)
      complete(HttpResponse(
        status = MethodNotAllowed,
        entity = s"Method not allowed. Supported methods are: ${names mkString " or "}"
      ))
    }
      .handleNotFound {
        complete(HttpResponse(status = NotFound, entity = "That API does not exist."))
      }
      .handle {
        case r: MalformedRequestContentRejection =>
          complete(HttpResponse(status = BadRequest, entity = s"Error reading limit parameter: ${r.message}"))
      }
      .result()

  val route: Route = handleRejections(rejectionHandler) {
    path("shout" / Segment) { twitterUserName =>
      get {
        parameters('limit.as[Int]) { limit =>
          handleExceptions(exceptionHandler) {
            complete {
              tweetShouter.shout(twitterUserName, limit)
            }
          }
        }
      }
    }
  }
}
