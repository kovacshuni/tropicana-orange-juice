package com.letgo.shoutingtweets.application

final case class UsernameMalformedException(message: String) extends RuntimeException(message)
