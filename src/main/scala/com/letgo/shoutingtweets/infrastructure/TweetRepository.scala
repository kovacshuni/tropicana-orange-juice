package com.letgo.shoutingtweets.infrastructure

import scala.concurrent.Future

import com.letgo.shoutingtweets.domain.Tweet

trait TweetRepository {
  def searchByUserName(username: String, limit: Int): Future[Seq[Tweet]]
}
