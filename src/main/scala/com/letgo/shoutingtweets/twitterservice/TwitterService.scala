package com.letgo.shoutingtweets.twitterservice

import scala.concurrent.Future

import com.letgo.shoutingtweets.domain.Tweet

trait TwitterService {
  def searchByUserName(username: String, count: Int): Future[Seq[Tweet]]
}
