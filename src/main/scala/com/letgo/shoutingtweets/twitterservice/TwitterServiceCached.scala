package com.letgo.shoutingtweets.twitterservice

import scala.concurrent.{ExecutionContext, Future}

import akka.http.caching.scaladsl.Cache
import org.slf4j.LoggerFactory

import com.letgo.shoutingtweets.domain.Tweet

class TwitterServiceCached(private val twitterService: TwitterService,
                           private val cache: Cache[String, Seq[Tweet]])
                          (implicit ec: ExecutionContext) extends TwitterService {

  private val logger = LoggerFactory.getLogger(getClass)

  def searchByUserName(username: String, count: Int): Future[Seq[Tweet]] = {
    val usersTweetsF = cache(username, () => {
        logger.debug(s"Cache miss. Calling underlying TwitterService. username=$username")
        twitterService.searchByUserName(username, count)
      })

    usersTweetsF.flatMap { usersTweets =>
      if (usersTweets.size < count) {
        logger.debug("Cache hit but cached result is shorter than expected. Evicting entry and re-trying. " +
          s"username=$username currentCount=${usersTweets.size} expectedCount=$count")
        cache.remove(username)
        searchByUserName(username, count)
      } else {
        Future.successful(usersTweets.take(count))
      }
    }
  }
}
