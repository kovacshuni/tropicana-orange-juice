package com.letgo.shoutingtweets.twitterservice

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

import com.letgo.shoutingtweets.domain.Tweet
import com.letgo.shoutingtweets.infrastructure.TweetRepository

class TwitterServiceLocal(private val tweetRepository: TweetRepository)
                         (implicit val ec: ExecutionContext) extends TwitterService {

  override def searchByUserName(username: String, count: Int): Future[Seq[Tweet]] =
    tweetRepository.searchByUserName(username, count). transform {
      case Success(s) => Success(s)
      case Failure(cause) =>
        Failure(TwitterServiceException("Failed to fetch tweets from repository. " +
          s"username=$username count=$count", cause))
    }
}
