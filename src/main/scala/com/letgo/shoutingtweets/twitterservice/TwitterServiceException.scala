package com.letgo.shoutingtweets.twitterservice

final case class  TwitterServiceException(private val message: String,
                                          private val cause: Throwable = None.orNull)
  extends RuntimeException(message, cause)
