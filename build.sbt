name := "shouting-tweets"

version := "0.1"

scalaVersion := "2.12.7"

scalacOptions ++= Seq(
  "-Ypartial-unification",
  "-language:postfixOps"
)

libraryDependencies ++= {
  val specs2Version = "4.3.4"

  Seq(
    "com.typesafe.akka" %% "akka-http" % "10.1.5",
    "com.typesafe.akka" %% "akka-actor" % "2.5.4",
    "com.typesafe.akka" %% "akka-stream" % "2.5.4",
    "com.typesafe.akka" %% "akka-http-caching" % "10.1.5",

    "org.slf4j" % "slf4j-api" % "1.7.26",
    "ch.qos.logback" % "logback-classic" % "1.2.3",

    "com.typesafe.akka" %% "akka-stream-testkit" % "2.5.4" % "test",
    "com.typesafe.akka" %% "akka-http-testkit" % "10.1.5" % "test",

    "org.specs2" %% "specs2-core" % specs2Version % Test,
    "org.specs2" %% "specs2-mock" % specs2Version % Test,
    "org.mockito" % "mockito-core" % "2.27.0" % Test
  )
}

addCommandAlias("c", "compile")
addCommandAlias("s", "scalastyle")
addCommandAlias("tc", "test:compile")
addCommandAlias("ts", "test:scalastyle")
addCommandAlias("t", "test")
addCommandAlias("to", "testOnly")
