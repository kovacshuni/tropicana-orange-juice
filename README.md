# Letgo Tech Test

## Description
We want you to implement a REST API that, given a twitter username and a count N, returns the last N tweets shouted. Shouting a tweet consists of transforming it to uppercase and adding an exclamation mark at the end. We also want to get a cache layer of these tweets in order to avoid hitting Twitter's API (which let's imagine is very expensive) twice for the same username given a T time.

## Example

Given these last two tweets from Donald Trump:
- "Big announcement with my friend Ambassador Nikki Haley in the Oval Office at 10:30am",
- "Will be going to Iowa tonight for Rally, and more! The Farmers (and all) are very happy with USMCA!"

The returned response should be:
```
curl -s http://oursuperawesometwittershoutapi.com/shout/realDonaldTrump?limit=2
[
    "BIG ANNOUNCEMENT WITH MY FRIEND AMBASSADOR NIKKI HALEY IN THE OVAL OFFICE AT 10:30AM!",
    "WILL BE GOING TO IOWA TONIGHT FOR RALLY, AND MORE! THE FARMERS (AND ALL) ARE VERY HAPPY WITH USMCA!"
]
```

## Constraints
- Count N provided MUST be equal or less than 10. If not, our API should return an error.

## Code Provided
In order to get you started, we've provided some bootstrap code.
- `ShoutController` and `Starter`: Web server done in Akka HTTP.
- `TweetRepository` and `TweetRepositoryInMemory`: We want you to work on domain code. Therefore, we provide an in-memory implementation of the Twitter repository that returns random quotes about science 🧐. You don't need to implement real communication with Twitter, you can use `TwitterRepositoryInMemory` as a production implementation.
- `Tweet` domain model: Simple case class for modeling Tweets returned by `TweetRepository`.

You can run the webserver by running `runMain com.letgo.scala_candidate_test.Starter` inside the sbt console. By default it runs on port 9000.

![hello-world](/doc/img/helloworld.png)

Feel free to change any of the provided code or do it by yourself if you feel more comfortable.

## What will we evaluate?
* **Design:** We know this is a very simple application and can be solved with one line of code but we want to see how you design domain code. Let's pretend this is a super critical application for the company and you're going to maintain it (and make changes requested by the product owner) for years.
* **Testing:** We love automated testing and we love reliable tests. We like testing for two reasons: First, good tests let us deploy to production without fear (even on a Friday!). Second, tests give a fast feedback cycle so developers in dev phase know if their changes are breaking anything.
* **Simplicity**: We like separate code in domain, application and infrastructure layers. If our product owner asks us for the same application but accessed by command line (instead of the http server) it should be super easy!

# Answer

This application returns the tweets of a person formatted to uppercase and an exclamation mark at the end.

This is an assignment exercise.

## How to use

Requires [sbt](https://www.scala-sbt.org) to be installed.

### Running

```
sbt run
```

### Using

```
curl "localhost:9000/shout/johndoe?limit=4"
[
	A MAN WITH A NEW IDEA IS A CRANK UNTIL HE SUCCEEDS!
	THE CREATIVE PERSON PAYS CLOSE ATTENTION TO WHAT APPEARS DISCORDANT AND CONTRADICTORY… AND IS CHALLENGED BY SUCH IRREGULARITIES!
	WHEN ADULTS FIRST BECOME CONSCIOUS OF SOMETHING NEW, THEY USUALLY EITHER ATTACK OR TRY TO ESCAPE FROM IT… ATTACK INCLUDES SUCH MILD FORMS AS RIDICULE, AND ESCAPE INCLUDES MERELY PUTTING OUT OF MIND!
	WE SHOULD BE ETERNALLY VIGILANT AGAINST ATTEMPTS TO CHECK THE EXPRESSION OF OPINIONS THAT WE LOATHE!
]
```

### Testing

```
sbt test
```

## My notes on the solution

1. I'm using another layer of TwitterService in TwitterServiceCached and TwitterServiceLocal
that connects to your TwitterRepository. It seems to be an unnecessary double layer,
but here is why I thought it's better:
    1. I find it's better to isolate our code from integrations. Define our interface when we
    are connecting to something. This case is special because it is a Scala trait integration,
    with which it would seem straightforward to use the given interface. But in case it was an
    HTTP API or a data deserialization process it would show more prominently that first we need
    to define ourselves what we are doing and then have an implementation for it. It's cleaner.
    1. It is easier to replace the integration. If you'd change `TweetRepository` to
    `FacebookPostRepository`, I could still use my interface.
    (disregarding the naming of the whole application)
    1. I don't know what exceptions might the underlying library throw. Moreover that there are no
    checked exceptions defined. If I only caught them on my top level, `ShoutController`, I would see
    a shorter stacktrace, immediately leading me to a possibly fuzzy reasons from below
    `TweetRepositoryInMemory`. I'd rather catch and wrap exceptions in the integration layer and
    decorate them to be more verbose and explain what happened.

1. Changing the application to a command-line one instead of a web server:
`ShoutController` would be rewritten, of course HTTP path, method handling, rejections would have to
 be the responsibility of the command-line client. The only thing I see debatable is the `exceptionHandler`
 that would need to be copy/pasted and integrated in the flow to catch the coming exceptions. I don't
 think it is better to push down logging and exception handling to a lower level, to any of the services
 or to `TweetShouter` or introducing another layer just for that. I would like those to be logged and caught
 on top level.

1. Caching: Users and their respective tweets can be cached, but since count is included in the request,
results can become quite discrepant.
Calling 2, then 3 tweets from the same person and in the meantime original tweets being updated
could yield in inconsistent responses. So I decided I'll cache a list of tweets per username and
if there are not enough tweets cached to serve the current call, I'll go to the underlying service,
replacing the current cached entry with a new, longer one. Only if it's long enough, I can use the cache.

1. Renamed the base package to be without underscores. I haven't found any reference to that using underscores is a convention.
I also considered to have a real name for the application.

1. Assumption: Since this is not a high load application I'm not going to define and separate multiple execution contexts.

1. Source file permissions had a weird setup having `-rwxr-xr-x` (e.g. `README.md`).
Was it just for me, due to email client, browser, archiving or OS X, I don't know but I changed them back to normal.

1. Username validation error messages could be even more specific but I'm resolving higher priority tasks first.

1. Deciding between case object or case class for CountExceededException, thinking case object would be better,
but Specs2 is giving me a hard time matching that with `throwsA[]` so I left it a case class for simplicity
and not writing a try/catch.
